## 4 A user can self register

## Status - Pending

## Description

  A user can register all the information they need to have access to their unit controlled personally.

  
## How I will know it worked

  I will not have to manually load the unit and user information on request.


## prerequisite
  [3](3-Auto-creation-of-a-twilio-number.md)