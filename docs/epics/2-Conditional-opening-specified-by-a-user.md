## 3  Auto creation of a twilio number

## Status - next sprint 

## Description

  A resident can compose rules that result in access being allowed to a visitor so that the have much greater control of who and when there apartment can be visted.

  
## How I will know it worked

  I will have everyone in my building paying a nominal fee. I will see no one using access fobs in the lift.



## Stories

### A resident can specify a delivery window so that a delivery can be securely made when the user is not home.


### A resident can create a code that when entered will allow access so that access can be given to trusted parties


### A resident can revoke a code so that they can remove access should a third party become un trusted


### A resident can specify a time range where access can be granted so that they can not be bothered


### a resident can specify a phone number to forward visitors to so that access can be granted by a specific person.


### A resident can combine and compose the previous stories so that they have maximal control of the access to thier apartment.
