# 2. Implement in single phoenix project with one module

Date: 2019-01-28 - Reviewed 2019-02-03 

## Status

Accepted

## Context

the bounded context of this application will remain rather contained. Any user interface will be a separate component. Elixir and Phoenix are relatively well supported and the benefit of a stateless functional restful api is worth trying out to ensure an optimal delivery via http.

## Decision

Use elixir and phoenix for core services

## Consequences

Using one project in place of a umbrella project has made Elixir Contexts for segregation of aggregate roots the obvious choice. I will have to keep an eye on the dependencies that have evolved between resources in separate contexts.    


