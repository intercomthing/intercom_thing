# 6. replace twiml builder with twiml library

Date: 2019-01-31 - Reviewed 2019-02-03 

## Status

Accepted

## Context

I had written a small builder that deals nicely with basic cases and was investigating macros for a dsl. [This library] (https://github.com/danielberkompas/ex_twiml) basically does what I would have had to re implement.

## Decision

Use https://github.com/danielberkompas/ex_twiml

## Consequences

Composing behaviors becomes more fluent.

Some non intuitive cases around the play verb where I just want to play the tone not content. 

I will re evaluate this as I would like to learn more about defining domain specific languages and Elixir macros. Improvements I make to solve my edge case more intuitively should be contributed back to the library.  


