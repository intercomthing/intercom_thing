# 3. main context will respond in xml other endpoints will be json

Date: 2019-01-29 - Reviewed 2019-02-03

## Status

Accepted

## Context

Twilio requires Twiml formatted responses so the endpoints that are accessed from the intercom must be xml

Phoenix does not provide first class support for xml responses

## Decision

Twilio called endpoints will be xml only, management endpoints will be json only.

The endpoint that Twilio sends a GET request two will use the elixir send_response and and put_content_type plug fallback.


## Consequences


I had to implement a specific assertion in my controller test that will be returning json response bodies to the twilio request when the intercom connects. If this is reused it should be moved into a test.   

