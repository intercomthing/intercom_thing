# 4. contribute XML mime types to phoenix project

Date: 2019-01-29 - Reviewed 2019-02-03

## Status

Suspended 

## Context

Phoenix does not natively support xml in its atom mime types

## Decision

The change that we're proposing or have agreed to implement. the time taken to get through the open source project will be greater then this application will be in active development

## Consequences

Will have to continue to use custom string for mime types.

I had to implement a custom method to verify xml content type in my controller unit test. 

I will re evaluate this at a later date when I have time to prepare a pull request back to the framework.


