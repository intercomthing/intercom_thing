# 5. implement access behaviour on one endpoint

Date: 2019-01-29 - Reviewed 2019-02-03 

## Status

Accepted

## Context

Twilio recieves the call from my intercom system it an be configured in many ways how to respond. Twiml XML is the DSL that Twilio uses to respond to the call.  

## Decision

Twilio will issue a get to one and only one endpoint an access token and number only. No fallback api will be available. The final fallback will be to forward the call to the unit owner. 

/api/access-behaviours/id:external_phone_number

[access-behavior-controllers](lib/intercom_thing_web/controllers/access_behaviour_controller)


## Consequences

Twilio will need to know very little about the internal workings but constraints will have to be built in to composible behaviours ensuring a successful open is possible.

Version 0.1 of this application provides only auto open and forwarding capability this has made the endpoint look less like a resource. 

A parameter to provide auto open behaviour has been added to this request. Future features will allow users to compose and customize what can cause the door to open at which point this will be removed.




