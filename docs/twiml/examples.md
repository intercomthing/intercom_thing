

<?xml version="1.0" encoding="UTF-8"?>
<Response>
  
  <!-- responseds with voice -- tokenize body 
    can be composed before any behaviour
  -->

    <Say> Please leave the delivery on level three</Say>
  

  <!-- hard coded already will open door 
    must be only or final active behaviour
    already implimented after story 3
  -->
  <Play digits="9">
  </Play>
  
  <!-- will need tokenized to and body -- from can be hard coded in mvp 
    probably below mvp for first iteration -- will require billing consideration
    probably must be subsequent to an open 
  -->
  <Sms to="+6421939570" from="+18323654673">
    door opened
  </Sms > 

  <!-- foward to another number tokenize number 
    must be final will end interaction
  -->
  <Dial>
    +64-21-939-570
  </Dial>


  <!--figure out how to listen for a tokenized code 
    probably needs time out and must be combined with a fallback behaviour
  -->


  <!-- firgure out how to set a wait-->

</Response> 