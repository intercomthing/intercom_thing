defmodule IntercomThing.ManageTest do
  use IntercomThing.DataCase

  alias IntercomThing.Manage

  describe "users" do
    alias IntercomThing.Manage.User

    @valid_attrs %{email_address: "some email_address", fall_back_phone_number: "some fall_back_phone_number", first_name: "some first_name", last_name: "some last_name"}
    @update_attrs %{email_address: "some updated email_address", fall_back_phone_number: "some updated fall_back_phone_number", first_name: "some updated first_name", last_name: "some updated last_name"}
    @invalid_attrs %{email_address: nil, fall_back_phone_number: nil, first_name: nil, last_name: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Manage.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Manage.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Manage.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Manage.create_user(@valid_attrs)
      assert user.email_address == "some email_address"
      assert user.fall_back_phone_number == "some fall_back_phone_number"
      assert user.first_name == "some first_name"
      assert user.last_name == "some last_name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Manage.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Manage.update_user(user, @update_attrs)
      assert user.email_address == "some updated email_address"
      assert user.fall_back_phone_number == "some updated fall_back_phone_number"
      assert user.first_name == "some updated first_name"
      assert user.last_name == "some updated last_name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Manage.update_user(user, @invalid_attrs)
      assert user == Manage.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Manage.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Manage.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Manage.change_user(user)
    end
  end
end
