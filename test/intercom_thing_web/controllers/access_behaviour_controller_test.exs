defmodule IntercomThingWeb.AccessBehaviourControllerTest do
  use IntercomThingWeb.ConnCase

  alias IntercomThing.Manage
  alias Plug.Conn


  @unit_attrs %{unit_id: "test",external_phone_number: "555555"}
  @user_attrs %{first_name: "test", last_name: "testerson", fall_back_phone_number: "5555444", email_address: "test@test.com"}


  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  def fixture(:unit) do
     {:ok, user} = Manage.create_user(@user_attrs)
     
     {:ok, unit} = @unit_attrs
      |> Enum.into(%{user_id: user.id})
      |> Manage.create_unit()
    
    unit
  end

  describe "with number" do
    setup [:create_unit]
    test "returns  xml", %{conn: conn} do
      conn = get(conn, Routes.access_behaviour_path(conn, :show, "555555", %{open: ""}))
      assert response(conn, 200)
      assert response_content_type_custom(conn, "text/xml; charset=utf-8")
    end
  end

  describe "with open" do
    setup [:create_unit]
    test "returns  xml", %{conn: conn} do
      conn = get(conn, Routes.access_behaviour_path(conn, :show, "555555", %{open: "open"}))
      assert response(conn, 200)
      assert response_content_type_custom(conn, "text/xml; charset=utf-8")
    end
  end  


@spec response_content_type_custom(Conn.t, atom) :: String.t | no_return
defp response_content_type_custom(conn, format)  do
  case Conn.get_resp_header(conn, "content-type") do
      [] ->
        raise "no content-type was set, expected a #{format} response"
      [h] ->
        h == format 
      [_|_] ->
        raise "more than one content-type was set, expected a #{format} response"
      end

  end

defp create_unit(_) do
    unit = fixture(:unit)
    {:ok, unit: unit}
  end
end
