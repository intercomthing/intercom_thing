defmodule IntercomThingWeb.Router do
  use IntercomThingWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", IntercomThingWeb do
    pipe_through :api
    resources "/access-behaviours", AccessBehaviourController, only: [:show]
  end
end
