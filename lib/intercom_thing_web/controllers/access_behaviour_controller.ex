defmodule IntercomThingWeb.AccessBehaviourController do
  use IntercomThingWeb, :controller
  import Plug.Conn

  alias IntercomThing.Main

  action_fallback IntercomThingWeb.FallbackController

  def show(conn, %{"id" => id, "open" => open}) do
    if (open === "open") do
      conn
        |> put_resp_content_type("text/xml")
        |> send_resp(200, Main.get_auto_open_access_behaviour(id).data)
    else
      conn
        |> put_resp_content_type("text/xml")
        |> send_resp(200, Main.get_fallback_behaviour(id).data)
    end
    
  end

end