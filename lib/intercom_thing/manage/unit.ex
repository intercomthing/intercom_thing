defmodule IntercomThing.Manage.Unit do
  use Ecto.Schema
  import Ecto.Changeset


  schema "units" do
    field :external_phone_number, :string
    field :unit_id, :string
    belongs_to :user, IntercomThing.Manage.User

    timestamps()
  end

  @doc false
  def changeset(unit, attrs) do
    unit
    |> cast(attrs, [:unit_id, :external_phone_number, :user_id])
    |> validate_required([:unit_id, :external_phone_number])
    |> unique_constraint(:unit_id)
  end
end
