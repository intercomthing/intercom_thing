defmodule IntercomThing.Manage.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :email_address, :string
    field :fall_back_phone_number, :string
    field :first_name, :string
    field :last_name, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name, :fall_back_phone_number, :email_address])
    |> validate_required([:first_name, :last_name, :fall_back_phone_number, :email_address])
  end
end
