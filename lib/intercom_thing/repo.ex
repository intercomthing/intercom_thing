defmodule IntercomThing.Repo do
  use Ecto.Repo,
    otp_app: :intercom_thing,
    adapter: Ecto.Adapters.Postgres
end
