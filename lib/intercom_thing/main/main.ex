defmodule IntercomThing.Main do
  @moduledoc """
  The module that will provide directives to the voip service that connects to the physical intercom
  """

  import ExTwiml
  import Ecto.Query, warn: false
  alias IntercomThing.Repo
  alias IntercomThing.Manage.Unit
  
  @doc """
  Gets twiml to open  and message 
  """
  def get_auto_open_access_behaviour(external_phone_number) do
    %{:data => twiml do
                  play digits: 9 do end
                  sms "door opened", to: get_unit_owner_fallback_number(external_phone_number), from: get_sender_phone_number()
                end} 
  end 

  @doc """
  Get twiml to foward call to the phone number specified on the unit owner
  """
  def get_fallback_behaviour(external_phone_number) do
    %{:data => twiml do
                  dial get_unit_owner_fallback_number(external_phone_number)
                end}
  end 

  defp get_unit_owner_fallback_number(external_phone_number) do 
    unit_with_owner = Repo.get_by(Unit |> preload(:user), external_phone_number: external_phone_number)
    unit_with_owner.user.fall_back_phone_number
  end

  defp get_sender_phone_number do
    Application.fetch_env!(:intercom_thing, :notifications_sender_number)
  end 


end
