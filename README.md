# IntercomThing

This is a resful api that will bridge between [twilio](https://www.twilio.com/what-is-cloud-communications?) telephony interchange and a phoneline based apartment intercom system allowing residents of my apartment building to be able to remotely control access to the building while maintaing physical security. 

It is designed to be accessed by multiple client applications the first of which will be a ember.js single page application


## For archtectural descision records [see](docs/architecture)

## Regular retorspective [blog/thoughts](docs/retrospectives)

## Epics planned [here](docs/epics)

Phoenix info

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix


Development prerequistes

  * a running instance of prostgres

  * postgres user as super user other config will not be checked into public repository

Production hosting TBA See archtectural descision records in repo for evolving thoughts  
  
  * packaging for production will be based on docker containers

  * probabaly red hat hosted

  * containerised postgres for persistant storage

  * due to an msdn licence I have hosting will probably be on MS azure  