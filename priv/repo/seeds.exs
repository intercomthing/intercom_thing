# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     IntercomThing.Repo.insert!(%IntercomThing.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias IntercomThing.Repo
alias IntercomThing.Manage.Unit
alias IntercomThing.Manage.User

Repo.delete_all User

Repo.delete_all Unit

Repo.insert! %Unit{
  external_phone_number: "6448880200",
  unit_id: "3C",
  user: %User{
    first_name: "Sam",
    last_name: "Dowsett",
    fall_back_phone_number: "640219395",
    email_address: "s.n.dowsett@gmail.com"
  }
}