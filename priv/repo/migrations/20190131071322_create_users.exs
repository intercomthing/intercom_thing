defmodule IntercomThing.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :first_name, :string
      add :last_name, :string
      add :fall_back_phone_number, :string
      add :email_address, :string

      timestamps()
    end

  end
end
