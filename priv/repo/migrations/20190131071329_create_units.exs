defmodule IntercomThing.Repo.Migrations.CreateUnits do
  use Ecto.Migration

  def change do
    create table(:units) do
      add :unit_id, :string
      add :external_phone_number, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:units, [:unit_id])
    create index(:units, [:user_id])
  end
end
