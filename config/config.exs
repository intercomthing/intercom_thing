# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :intercom_thing,
  ecto_repos: [IntercomThing.Repo]

# Configures the endpoint
config :intercom_thing, IntercomThingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aT3Wg0zR+YmB8ER11rcbdswRmBha6qVCDI6JnE1/n9f4ZbeCxwrW7a2fX73183vi",
  render_errors: [view: IntercomThingWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: IntercomThing.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason
#configure general stuff
config :intercom_thing, 
    notifications_sender_number: 18323654673

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
